const request = require('request-promise')
const Sequelize = require("sequelize")
const config = require("../config")
const {database, username, password, host} = config.mysql
const sequelize = new Sequelize(database, username, password, {
    host,
    dialect: "mysql",
    pool: {
        max: 10,
        min: 0,
        idle: 10000
    },
    define: {
        timestamps: false
    }
})
const NewsModel = require("../models/news")(sequelize, Sequelize)
module.exports = (async () => {
    const HOST = 'https://ow.blizzard.cn'
    const URL = `${HOST}/action/article/list`
    const {data: {list}} = JSON.parse(await request(`${URL}?p=1&pageSize=100000&t=${Date.now()}`))
    const data = list.map((item, index) => {
        return {
            id: index + 1,
            title: item.title,
            url: HOST + item.linkUrl,
            description: item.description,
            imagine: item.thumbnailUrl,
            publishTime: item.publishTime
        }
    })
    await NewsModel.destroy({
        where: {},
        truncate: true
    })
    console.log(data)
    await Promise.all(data.map(async item => {
        return NewsModel.create({
            ID: item.id,
            TITLE: item.title,
            URL: item.url,
            DESCRIPTION: item.description,
            IMAGINE: item.imagine,
            PUBLISHTIME: item.publishTime
        })
    }))
})

