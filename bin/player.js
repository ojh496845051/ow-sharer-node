const puppeteer = require('puppeteer');
const cherrio = require("cheerio")
const Sequelize = require("sequelize")
const config = require("../config")
const roleMap = {
    '支援': 'SUPPORT',
    '重装': 'TANK',
    '输出': 'DAMAGE'
}
module.exports = async () => {

    const browser = await puppeteer.launch({
        headless: false,   //有浏览器界面启动
        slowMo: 100,       //放慢浏览器执行速度，方便测试观察
        args: [            //启动 Chrome 的参数，详见上文中的介绍
            '–no-sandbox',
            '--window-size=1980,1020'
        ],
    });
    const page = await browser.newPage();

    page.goto('https://www.overwatchleague.cn/zh-cn/players', {
        timeout: 60 * 60 * 1000
    });

    let hasNext = true;
    let playerList = []

    do {
        await page.waitForSelector(".players-liststyles__PlayerRow-sc-1jhwo3g-23", {
            timeout: 60 * 60 * 1000
        })
        await page.waitFor(1 * 1000)
        const list = await page.$$eval(".players-liststyles__PlayerRow-sc-1jhwo3g-23", list => {
            return list.map(item => item.outerHTML)
        })
        await Promise.all(list.map(async html => {
            const $ = cherrio.load(html)
            const name = $($(".resizable-namestyles__Abbreviation-sc-1qh8dp6-1")[0]).text()
            const headshot = $(".table-cardstyles__Picture-sc-2s08up-2").text();
            const team = $($(".resizable-namestyles__Abbreviation-sc-1qh8dp6-1")[1]).text()
            const realname = $(".players-liststyles__Link-sc-1jhwo3g-14").text()
            const hometown = $(".players-liststyles__HometownCell-sc-1jhwo3g-8").text().replace(/\s/g, "")
            const role = roleMap[$(".players-liststyles__RoleName-sc-1jhwo3g-20").text()]
            playerList.push({
                name,
                headshot,
                team,
                realname,
                hometown,
                role
            })
        }))
        console.log('爬取当前页成功');

        await page.waitForSelector(".paginationstyles__ContainerNumbers-sc-1u14940-2")
        const pageHtml = await page.$$eval(".paginationstyles__ContainerNumbers-sc-1u14940-2", list => {
            return list[0].outerHTML
        })

        const $ = cherrio.load(pageHtml)
        const pageList = Array.from($(".paginationstyles__Number-sc-1u14940-7"))
        hasNext = $(pageList[pageList.length - 1]).attr("color") === "#333333"
        console.log('准备进入下一页');
        if (hasNext) {
            await page.evaluate(() => {
                const list = Array.from(document.querySelectorAll(".paginationstyles__Number-sc-1u14940-7"))
                const activeIndex = list.findIndex(item => item.getAttribute("color") !== "#333333")
                list[activeIndex+1].click()
            })
        }
    } while(hasNext)

    hasNext = true

    page.goto('https://www.overwatchleague.cn/zh-cn/stats', {
        timeout: 60 * 60 * 1000
    });

    do {
        await page.waitForSelector(".table-rowstyles__Content-sc-1t1l9w4-3", {
            timeout: 60 * 60 * 1000
        })
        await page.waitFor(1 * 1000)
        const list = await page.$$eval(".table-rowstyles__Content-sc-1t1l9w4-3", list => {
            return list.map(item => item.outerHTML)
        })


        await Promise.all(list.map(async html => {
            const $ = cherrio.load(html)
            const headshot = $(".table-cardstyles__Picture-sc-2s08up-2").attr("src");
            const name = $($(".resizable-namestyles__Name-sc-1qh8dp6-2")[0]).text()
            const team = $($(".resizable-namestyles__Name-sc-1qh8dp6-2")[1]).text()
            const elim = $($(".statistics-tablestyles__TableData-sc-68hghq-5")[0]).text()
            const deaths = $($(".statistics-tablestyles__TableData-sc-68hghq-5")[1]).text()
            const damage = $($(".statistics-tablestyles__TableData-sc-68hghq-5")[2]).text()
            const healing = $($(".statistics-tablestyles__TableData-sc-68hghq-5")[3]).text()
            const timeplayed = $($(".statistics-tablestyles__TableDataTimePlayed-sc-68hghq-6")).text()
            
            const targetIndex = playerList.findIndex(item => item.name === name)
            const newPlayer = {
                name,
                team,
                elim,
                deaths,
                damage,
                healing,
                timeplayed,
                headshot
            }
            if (targetIndex > -1) {
                playerList[targetIndex] = {
                    ...playerList[targetIndex],
                    ...newPlayer
                }
            } else {
                playerList.push(newPlayer)
            }
            
        }))
        console.log('爬取当前页成功');

        await page.waitForSelector(".paginationstyles__ContainerNumbers-sc-1u14940-2")
        const pageHtml = await page.$$eval(".paginationstyles__ContainerNumbers-sc-1u14940-2", list => {
            return list[0].outerHTML
        })

        const $ = cherrio.load(pageHtml)
        const pageList = Array.from($(".paginationstyles__Number-sc-1u14940-7"))
        hasNext = $(pageList[pageList.length - 1]).attr("color") === "#333333"
        console.log('准备进入下一页');
        if (hasNext) {
            await page.evaluate(() => {
                const list = Array.from(document.querySelectorAll(".paginationstyles__Number-sc-1u14940-7"))
                const activeIndex = list.findIndex(item => item.getAttribute("color") !== "#333333")
                list[activeIndex+1].click()
            })
        }
    } while (hasNext)

    const {database, username, password, host} = config.mysql
    const sequelize = new Sequelize(database, username, password, {
        host,
        dialect: "mysql",
        pool: {
            max: 10,
            min: 0,
            idle: 10000
        },
        define: {
            timestamps: false
        }
    })
    const PlayerModel = require("../models/player")(sequelize, Sequelize)
    await PlayerModel.destroy({
        where: {},
        truncate: true
    })
    await Promise.all(playerList.map(async (item, index) => {
        return await PlayerModel.create({
            ID: index + 1,
            NAME: item.name,
            HEADSHOT: item.headshot,
            REALNAME: item.realname,
            ROLE: item.role,
            TEAM: item.team,
            ELIM: item.elim,
            DEATHS: item.deaths,
            DAMAGE: item.damage,
            HEALING: item.healing,
            TIMEPLAYED: item.timeplayed,
            HOMETOWN: item.hometown
        })
    }))
    // await page.close();
    // await browser.disconnect();
}