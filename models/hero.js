/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hero', {
    ID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ROLE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    NAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IMAGINE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FULLNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    PROFESSION: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    BASE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ARMY: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SAYING: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    STORY: {
      type: DataTypes.STRING(2000),
      allowNull: true
    },
    DIFFICULTY: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    DESCRIPTION: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'hero'
  });
};
