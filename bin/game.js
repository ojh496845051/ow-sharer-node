const puppeteer = require("puppeteer");
const cherrio = require("cheerio");
const Sequelize = require("sequelize");
const config = require("../config");
const { database, username, password, host } = config.mysql;

module.exports = async () => {
  const sequelize = new Sequelize(database, username, password, {
    host,
    dialect: "mysql",
    pool: {
      max: 10,
      min: 0,
      idle: 10000,
    },
    define: {
      timestamps: false,
    },
  });
  const GameModel = require("../models/game")(sequelize, Sequelize);
  const TeamModal = require("../models/team")(sequelize, Sequelize);
  await GameModel.destroy({
    where: {},
    truncate: true,
  });
  await TeamModal.destroy({
    where: {},
    truncate: true,
  });
  const browser = await puppeteer.launch({
    headless: false, //有浏览器界面启动
    args: [
      //启动 Chrome 的参数，详见上文中的介绍
      "--window-size=1980,1020",
    ],
  });
  const page = await browser.newPage();
  page.goto(`https://www.overwatchleague.cn/zh-cn/schedule`, {
    timeout: 60 * 60 * 1000,
  });
  await page.waitForSelector(".tabsstyles__Link-sc-1fl2yza-6", {
    timeout: 60 * 60 * 1000,
  });
  const html = await page.evaluate(() => {
    return document.querySelectorAll(
      ".tabsstyles__Labels-sc-1fl2yza-3.itQpah"
    )[1].outerHTML;
  });
  const $ = cherrio.load(html);
  const length = Array.from(
    $(".tabsstyles__Label-sc-1fl2yza-4")
  ).filter((item) => /^\d+$/.test($(item).text())).length;

  let counter = 0;
  let teamMap = new Map();

  page.on("response", async (response) => {
    if (
      response
        .url()
        .startsWith(
          "https://35u5wdmjah.execute-api.us-west-2.amazonaws.com/production/owl/paginator/schedule"
        )
    ) {
      const data = await response.json();
      const weebnumber = data.content.tableData.pagination.currentPage;
      const raceType = data.content.meta.tabs[0].name;
      try {
        const games = data.content.tableData.events
          .map(({ matches }) => {
            return matches.map((item) => {
              let {
                startDate,
                endDate,
                id,
                scores,
                status,
                competitors: [team1, team2],
                broadcastChannels,
                venue: { name: venue },
              } = item;
              scores = JSON.stringify(scores);
              const link =
                Array.isArray(broadcastChannels) && broadcastChannels.length
                  ? broadcastChannels[0].link.href
                  : "";
              teamMap.set(team1.id, team1);
              teamMap.set(team2.id, team2);
              return {
                startDate,
                raceType,
                endDate,
                id,
                scores,
                status,
                link,
                venue,
                weebnumber,
                teamId1: team1.id,
                teamId2: team2.id,
              };
            });
          })
          .flat();

        await Promise.all(
          games.map((item) => {
            return GameModel.create({
              ID: item.id,
              SCORES: item.scores,
              STATUS: item.status,
              RACETYPE: item.raceType,
              WEEKNUMBER: item.weebnumber,
              VENUE: item.venue,
              STARTTIME: item.startDate,
              ENDTIME: item.endDate,
              LINK: item.link,
              TEAMID1: item.teamId1,
              TEAMID2: item.teamId2,
            });
          })
        );

        checkOver();
      } catch (e) {
        checkOver();
        console.log(e);
      }
    }
  });
  for (let i = 1; i <= length; i++) {
    await page.evaluate((i) => {
      console.log(
        document
          .querySelectorAll(".tabsstyles__Labels-sc-1fl2yza-3.itQpah")[1]
          .childNodes[i].childNodes[0].click()
      );
    }, i);
    await page.waitFor(1000);
  }

  async function checkOver() {
    counter++;
    console.log(counter);

    if (counter === length) {
      const values = [];
      teamMap.forEach(value => {
        values.push(value);
      });

      await Promise.all(
        values.map((item) => {
          return TeamModal.create({
            ID: item.id,
            ICON: item.icon,
            LOGO: item.logo,
            NAME: item.name,
            ABBREVIATENAME: item.abbreviatedName,
          });
        })
      );
      // await page.close(); 
      // await browser.disconnect();
    }
  }
};
