/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('game', {
    ID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    SCORES: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    STATUS: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    RACETYPE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    WEEKNUMBER: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    VENUE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    STARTTIME: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ENDTIME: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LINK: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TEAMID1: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TEAMID2: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'game'
  });
};
