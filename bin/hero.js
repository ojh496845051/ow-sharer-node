const request = require('request-promise')
const cherrio = require("cheerio")
const config = require("../config")
const Sequelize = require("sequelize")
const {database, username, password, host} = config.mysql
const sequelize = new Sequelize(database, username, password, {
    host,
    dialect: "mysql",
    pool: {
        max: 10,
        min: 0,
        idle: 10000
    },
    define: {
        timestamps: false
    }
})

const HeroModel = require("../models/hero")(sequelize, Sequelize)
const SkillModel = require("../models/skill")(sequelize, Sequelize)

const URL = "https://ow.blizzard.cn/heroes/"

module.exports = async () => {
    await HeroModel.destroy({
        where: {},
        truncate: true
    })
    await SkillModel.destroy({
        where: {},
        truncate: true
    })
    const html = await request(URL)
    const $ = cherrio.load(html)
    let skillId = 1;
    await Promise.all(Array.prototype.map.call($("#heroes-selector-container .hero-portrait-detailed-container"), async (item, index) => {
        const role = $(item).attr("data-groups").replace(/\W/g, "");
        const imagine = $(item).find(".portrait").attr("src");
        const name = $(item).find(".portrait-title").text();
        const detailHref = URL.replace(/\/heroes\/$/, $(item).find(".hero-portrait-detailed").attr("href"))
        const detailHtml = await request(detailHref)
        const $d = cherrio.load(detailHtml)
        const difficulty = $d(".hero-detail-difficulty .star").filter((index, item) => {
            return $d(item).attr("class").indexOf("m-empty") === -1
        }).length;
        const description = $d(".hero-detail-description").text()
        const fullname = $d(".hero-bio .name").text().trim().replace(/^(全称|本名)\：/, "")
        const profession = $d(".hero-bio .occupation").text().trim().replace(/^(职业)\：/, "")
        const base = $d(".hero-bio .base").text().trim().replace(/^(行动基地)\：/, "")
        const army = $d(".hero-bio .affiliation").text().trim().replace(/^(隶属)\：/, "")
        const saying = $d(".hero-detail-title").text().trim().replace(/(角色类型难度技能简介)/, "")
        const story = Array.prototype.map.call($d(".hero-bio-backstory p"), (item) => {
            return $(item).text()
        }).join("\n");
        const skill = Array.prototype.map.call($d(".hero-ability"), item => {
            const imagine = $(item).find(".hero-ability-icon-bg img").attr("src")
            const name = $(item).find(".hero-ability-descriptor h4").text()
            const description = $(item).find(".hero-ability-descriptor p").text()
            const skillvideo = $(item).find(".hero-ability-video source").attr("src")
            return {
                name,
                imagine,
                description,
                skillvideo
            }
        })
        const heroId = index + 1
        await HeroModel.create({
            ID: heroId,
            ROLE:role,
            NAME: name,
            IMAGINE: imagine,
            FULLNAME: fullname,
            DESCRIPTION: description,
            PROFESSION: profession,
            BASE: base,
            ARMY: army,
            SAYING: saying,
            STORY: story,
            DIFFICULTY: difficulty
        })
        await Promise.all(skill.map(async item => {
            return await SkillModel.create({
                ID: skillId++,
                HERO_ID: heroId,
                NAME: item.name,
                IMAGINE: item.imagine,
                DESCRIPTION: item.description,
                SKILLVIDEO: item.skillvideo
            })
        }))
    }))
}