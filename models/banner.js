/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('banner', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    jumplink: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'banner'
  });
};
