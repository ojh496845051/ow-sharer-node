/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('player', {
    ID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    REALNAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ROLE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    NAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TEAM: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ELIM: {
      type: "DOUBLE(255,2)",
      allowNull: true
    },
    DEATHS: {
      type: "DOUBLE(255,2)",
      allowNull: true
    },
    DAMAGE: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    HEALING: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    TIMEPLAYED: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TEAMID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    HOMETOWN: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TEAMIMAGINE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TEAMABBREVIATION: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    TOPHERO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    HEADSHOT: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'player'
  });
};
