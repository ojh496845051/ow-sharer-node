/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('activity', {
    ID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    TITLE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    URL: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'activity'
  });
};
