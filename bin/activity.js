const request = require('request-promise')
const Sequelize = require("sequelize")
const cherrio = require('cheerio');
const config = require("../config")
const {database, username, password, host} = config.mysql
module.exports = (async () => {
    const sequelize = new Sequelize(database, username, password, {
        host,
        dialect: "mysql",
        pool: {
            max: 10,
            min: 0,
            idle: 10000
        },
        define: {
            timestamps: false
        }
    })
    const ActivityModel = require("../models/activity")(sequelize, Sequelize)
    const HOST = `https://ow.blizzard.cn`
    const URL = `${HOST}/home`
    const html =  await request(URL)
    const $ = cherrio.load(html);
    const list = $($(".m-sub-menu")[1]).children();
    console.log(list);
    
    const data = Array.from(list).map((item, index) => {
        const url = HOST + $($(item).children()[0]).attr("href")
        const title = $(item).text()
        return {
            id: index + 1,
            url,
            title
        }
    })
    await ActivityModel.destroy({
        where: {},
        truncate: true
    })
    console.log(data)
    await Promise.all(data.map(async item => {
        return ActivityModel.create({
            ID: item.id,
            TITLE: item.title,
            URL: item.url
        })
    }))
    
    
    
    
    // const data = list.map((item, index) => {
    //     return {
    //         id: index + 1,
    //         title: item.title,
    //         url: HOST + item.linkUrl,
    //         description: item.description,
    //         imagine: item.thumbnailUrl,
    //         publishTime: item.publishTime
    //     }
    // })
    
})

