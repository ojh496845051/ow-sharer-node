/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('skill', {
    HERO_ID: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    ID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    NAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    DESCRIPTION: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    IMAGINE: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    SKILLVIDEO: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'skill'
  });
};
