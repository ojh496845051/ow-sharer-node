/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('team', {
    ID: {
      type: DataTypes.INTEGER,
      allowNull: true,
      primaryKey: true
    },
    ICON: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    LOGO: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    NAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ABBREVIATENAME: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'team'
  });
};
